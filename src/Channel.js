import El from "./El";
import $ from "jquery";
import WaveSurfer from "./WaveSurfer";
import getAudioContext from "./getAudioContext";
import ToggleButton from "./ToggleButton";
const context=getAudioContext();

const Channel=function(){
    const self=this;
    let isPlaying=false;
    let normalize=false;
    let envelope=false;
    let tesselate=false;
    const label=(["A","B","C","D"])[Channel.list.length];
    Channel.list.push(this);
    El.call(this,`<div class="channel"></div>`);
    // const self=this;
    const $picture=$(`<img class="bg" src="./media/button-${label}.png"/>`);
    const $button=$(`<div class="graph-button"></div>`);
    const $label=$(`<p class="label-button">${label}</p>`);

    let svgWidth=500;
    let svgHeight=40;

    const $svgContainer=$(`
        <svg class="wave" viewBox="0 0 ${svgWidth} ${svgHeight}">
            <path
                d="M2 1 h1 v1 h1 v1 h-1 v1 h-1 v-1 h-1 v-1 h1 z" />
        </svg>`);
    const $path=$svgContainer.find("path");

    var $slider=$(`<input data-action="vol" step="0.01" type="range" min="0" max="2" value="0.02" style="width: 100%">`);

    var $controlContainer=$(`<div></div>`);
    var $useNormalize=new ToggleButton('Normalize');
    var $useXcrossingButton=new ToggleButton('Xcrossing');
    var $useTesselateButton=new ToggleButton('Tesselate');
    var $useEnvelopeButton=new ToggleButton('Envelope');

    $controlContainer.append([
        $useXcrossingButton.$el,
        $useTesselateButton.$el,
        $useEnvelopeButton.$el,
        $useNormalize.$el,
    ]);

    $useNormalize.on("toggled",function(newValue){
        normalize=newValue;
        self.set();
    });
    $useEnvelopeButton.on("toggled",function(newValue){
        envelope=newValue;
        self.set();
    });
    $useTesselateButton.on("toggled",function(newValue){
        tesselate=newValue;
        self.set();
    });

    $controlContainer.appendTo(self.$el);

    $slider.on('input',function (evt) {
        // console.log(evt);
        changeVolume(Number(this.value));
    });

    $slider.appendTo(self.$el);
    $button.appendTo(self.$el);
    $picture.appendTo($button);
    $label.appendTo($button);
    $path.appendTo($svgContainer);
    $svgContainer.appendTo(this.$el);

    let svgString="";

    const myGain = context.createGain();
    myGain.connect(context.destination);

    function changeVolume(to){
        console.log("volume",to);
        // myGain.gain.setValueAtTime(0, to);
        myGain.gain.value=to;
    }
    function updateSvg(array){
        
        let length=array.length;
        let widthPerVal=svgWidth/length;
        let step=Math.max(1,widthPerVal);
        const multiply=1;

        svgString=`M0,${svgHeight/2}`;

        for(let index=0; index<length; index+=step){
            let x=index*widthPerVal;
            svgString+=`\nL${x},${array[index]*multiply*svgHeight/2+svgHeight*0.5}`;
        }

        $path.attr("d",svgString);
    }

    $path.attr("d",svgString);
    /**
     * @type {AudioBufferSourceNode|false}
     */
    var source=false;
    /**
     * @type {number[]|false}
     */
    var currentAudioArray=false;
    var currentSampleRate=44100;

    this.set=function(
        audioArray=currentAudioArray,
        sampleRate=currentSampleRate
    ){
        if(!audioArray) return;


        if(tesselate){
            const length=audioArray.length;
            const halfLength=Math.round(length/2);
            const audioCopy=audioArray.slice(0);
            for(let a=0; a<length;a++){
                const alevel=Math.cos(Math.PI * a/halfLength)+1;
                const level=1-Math.cos(Math.PI * a/halfLength);
                audioArray[a]*=level;

                if(a<halfLength){
                    audioArray[a]+=audioCopy[a+halfLength]*alevel;
                }else if(a>halfLength){
                    audioArray[a]+=audioCopy[a-halfLength]*alevel;
                }
            }
        }
        if(normalize){
            const multiply=
                1/
                Math.max(
                    Math.max(...audioArray),
                    Math.abs(Math.min(...audioArray))
                );
            audioArray=audioArray.map((a)=>a*multiply);
        }
        if(envelope){
            const envelopeLength=400;
            for(let a=0; a<envelopeLength;a++){
                audioArray[a]*=a/envelopeLength;
                audioArray[audioArray.length-a]*=a/envelopeLength;
            }
        }


        currentAudioArray=audioArray;

        updateSvg(audioArray);

        const b = new AudioBuffer({
            length: audioArray.length,
            sampleRate: sampleRate,
            numberOfChannels: 1,
        });

        b.copyToChannel(new Float32Array(audioArray),0,0);

        if(source){
            try{
                source.disconnect();
                source.stop();
            }catch(e){}
        }

        source = new AudioBufferSourceNode(context, {buffer: b});
        if(!source) throw new Error("failed to make source");
        source.connect(myGain);
        source.loop = true;

        if(isPlaying){
            console.log("restart");
            isPlaying=true;
            source.start(0);
        }
    }
    $button.on("click",()=>{
        if(isPlaying){
            console.log("stop");
            isPlaying=false;
        }else{
            console.log("start");
            isPlaying=true;
        }
        self.set();
        $button.removeClass((!isPlaying)+"");
        $button.addClass((isPlaying)+"");
    });

}
Channel.list=[];

export default Channel;
