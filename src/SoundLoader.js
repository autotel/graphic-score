import El from "./El";
import getAudioContext from "./getAudioContext";

const context=getAudioContext();

var SoundLoader=function(){
    El.call(this,`<input type="file" accept="audio/*">`);
    const self=this;
    this.$el[0].addEventListener('change', function(e) {  
        var reader = new FileReader();
        reader.onload = function(result) {
            const audioArrayBuffer=result.target.result;
            self.emit("read",audioArrayBuffer);
            console.log(result,audioArrayBuffer);            
            context.decodeAudioData(audioArrayBuffer, function(decodedAudio) {
                self.emit("decode",decodedAudio);
                self.emit("load",{audioArrayBuffer,decodedAudio});
            }, function () { console.error('The request failed.'); } );
        };
        reader.readAsArrayBuffer(this.files[0]);
    }, false);
}

export default SoundLoader;
