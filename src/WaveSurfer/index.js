
import El from '../El.js';
import $ from "jquery";
import WaveSurfer from './wavesurfer.js';
import WaveSurferRegions from './wavesurfer.regions.min.js';
WaveSurfer.regions=WaveSurferRegions;
var Wave=function(){
    const self=this;
    const unique="wavesurfer-"+Wave.list.length;
    Wave.list.push(this);
    El.call(this,`<div id=${unique} class="waveform"></div>`);
    let waveSurfer=false;
    const superAppendTo=this.appendTo;
    this.appendTo=function(...p){
        superAppendTo(...p);
        waveSurfer = WaveSurfer.create({
            container: `#${unique}`,
            waveColor: 'violet',
            progressColor: 'purple',
            plugins: [
                WaveSurfer.regions.create({
                    regions: [
                        {
                            start: 0,
                            end: 0.1,
                            loop: false,
                            color: 'rgb(117, 219, 206)',
                            id:'A',
                        }, 
                        {
                            start: 0,
                            end: 0.1,
                            loop: false,
                            color: 'rgb(219, 117, 185)',
                            id:'B',
                        }, 
                    ],
                })
            ]
        });
        
        waveSurfer.on('region-created',
            (...p)=>self.emit("region-created",...p)
        );
        waveSurfer.on('region-updated',
            (...p)=>self.emit("region-updated",...p)
        );
        waveSurfer.on('region-update-end',
            (...p)=>self.emit("region-update-end",...p)
        );
        waveSurfer.on('region-removed',
            (...p)=>self.emit("region-removed",...p)
        );
        waveSurfer.on('region-play',
            (...p)=>self.emit("region-play",...p)
        );
        waveSurfer.on('region-in',
            (...p)=>self.emit("region-in",...p)
        );
        waveSurfer.on('region-out',
            (...p)=>self.emit("region-out",...p)
        );
        waveSurfer.on('region-mouseenter',
            (...p)=>self.emit("region-mouseenter",...p)
        );
        waveSurfer.on('region-mouseleave',
            (...p)=>self.emit("region-mouseleave",...p)
        );
        waveSurfer.on('region-click',
            (...p)=>self.emit("region-click",...p)
        );
        waveSurfer.on('region-dblclick',
            (...p)=>self.emit("region-dblclick",...p)
        );
        var $slider=$(`<input data-action="zoom" type="range" min="20" max="20000" value="0" style="width: 100%">`);
        $slider.on('input',function (evt) {
            console.log(evt);
            waveSurfer.zoom(Number(this.value));
        });
        $slider.appendTo(self.$el);
        

        return self;
    }
    this.set=function(buffer){
        if(!waveSurfer) return;
        var blob = new window.Blob([new Uint8Array(buffer)]);
        waveSurfer.loadBlob(blob);
        return self;
    }


    

}
Wave.list=[];

export default Wave;