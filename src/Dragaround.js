/**
 * this code is annoying to make, please keep it reusable
 */
const Dragaround=function(domEl){
    let handles=new Set();

    
    let Handle=function(props){
        let self=this;
        let myEl=document.createElement("div");


        myEl.className="dragaround-handle";
        let position={x:0,y:0};
        let clickPosition={x:0,y:0};
        domEl.appendChild(myEl);
        if(props.x!==undefined) position.x=props.x;
        if(props.y!==undefined) position.y=props.y;
        if(props.width!==undefined) myEl.style.width=props.width+"px";
        if(props.height!==undefined) myEl.style.height=props.height+"px";
        updatePosition();

        let exported=this.exported={};
        exported.position=position;
        exported.onDragStart=(pos)=>{}
        exported.onDragEnd=(pos)=>{}
        exported.onDrag=(pos)=>{}
        exported.reposition=(newPos)=>{
            position.x=newPos.x;
            position.y=newPos.y;
            updatePosition();
        }
        exported.domElement=domEl;
        exported.startDragging=()=>{
            self.dragStartHandler();
        }
        exported.remove=()=>{
            mouse.isDragging.delete(self);
            handles.delete(self);
            myEl.remove();
        }
        function updatePosition(){
            myEl.style.left=position.x+"px";
            myEl.style.top=position.y+"px";
        }
        this.dragStartHandler=(evt)=>{
            domEl.classList.add("select");
            mouse.isDragging.add(self);
            clickPosition={
                x:position.x-mouse.x,
                y:position.y-mouse.y,
            }
            exported.onDragStart(position);
        }
        this.dragEndHandler=()=>{
            domEl.classList.remove("select");
            mouse.isDragging.delete(this);
            position.x=mouse.x+clickPosition.x;
            position.y=mouse.y+clickPosition.y;
            updatePosition();
            exported.onDragEnd(position);
        }
        this.dragHandler=()=>{
            position.x=mouse.x+clickPosition.x;
            position.y=mouse.y+clickPosition.y;
            
            updatePosition();
            exported.onDrag(position);
        }
        myEl.addEventListener("mouseenter",(evt)=>{
            mouse.isHovering=(self);
        });
        myEl.addEventListener("mouseleave",(evt)=>{
            mouse.isHovering=false;
        });
        
        
    }
    this.createHandle=(props)=>{
        let newHandle=new Handle(props);
        handles.add(newHandle);
        return newHandle.exported;
    }
    let mouse=this.mouse={
        x:0,y:0,
        leftButtonPressed:false,
        rightButtonPressed:false,
        //whether handles are being dragged
        isDragging:new Set(),
        //whether a handle is under the mouse (always one, but set for consistency)
        isHovering:false,
        //whether a group of handles has been selected by an area
        isGroupSelected:new Set(),
        //whether the mouse is making an area selection
        isSelectingArea:false,
    }


    let areaElement=document.createElement("div");
    areaElement.classList.add("area-element");


    const areaSelectionStart=()=>{
        console.log("selStart");
        mouse.isSelectingArea=true;
        domEl.appendChild(areaElement);
        areaElement.style.left=mouse.x+"px";
        areaElement.style.top=mouse.y+"px";
        areaElement.style.border="solid 1px";
        areaElement.style.position="fixed";
    };
    const areaSelectionMove=()=>{
        let x0=Math.min(mouse.x,mouse.leftButtonPressed.x);
        let y0=Math.min(mouse.y,mouse.leftButtonPressed.y);
        let x1=Math.max(mouse.x,mouse.leftButtonPressed.x);
        let y1=Math.max(mouse.y,mouse.leftButtonPressed.y);
        let w=x1-x0;
        let h=y1-y0;

        areaElement.style.left=x0+"px";
        areaElement.style.top=y0+"px";
        areaElement.style.width=w+"px";
        areaElement.style.height=h+"px";

        handles.forEach((handle)=>{
            if(
                handle.exported.position.x>x0 && handle.exported.position.x<x1 &&
                handle.exported.position.y>y0 && handle.exported.position.y<y1
            ){
                mouse.isGroupSelected.add(handle);
            }else{
                mouse.isGroupSelected.delete(handle);
            }
        });

    };
    const areaSelectionEnd=()=>{


        mouse.isSelectingArea=false;

        areaElement.style.left=0+"px";
        areaElement.style.top=0+"px";
        areaElement.style.width=0+"px";
        areaElement.style.height=0+"px";

        if(domEl.contains(areaElement)){
            domEl.removeChild(areaElement);
        }


    }


    domEl.addEventListener("mousedown", (evt)=>{
        if(event.button==0){
            mouse.leftButtonPressed={x:mouse.x,y:mouse.y}
        }else if(event.button==2){
            mouse.rightButtonPressed={x:mouse.x,y:mouse.y}
        }
        if(mouse.isDragging.size==0 && mouse.isHovering==false){
            areaSelectionStart();
            mouse.isGroupSelected.clear();
        }else if(mouse.isGroupSelected.has(mouse.isHovering)){
            mouse.isGroupSelected.forEach((handle)=>{
                handle.dragStartHandler();
            });
        }else if(mouse.isHovering){
            mouse.isGroupSelected.clear();
            evt.stopPropagation();
            evt.preventDefault();
            mouse.isHovering.dragStartHandler();
        }else{
            mouse.isGroupSelected.clear();
        }
        evt.preventDefault();
    });
    domEl.addEventListener("mouseup", (evt)=>{
        mouse.isDragging.forEach((handle)=>{
            handle.dragEndHandler();
        });
        mouse.isDragging.clear();
        if(event.button==0){
            mouse.leftButtonPressed=false;
            areaSelectionEnd();
        }else if(event.button==2){
            mouse.rightButtonPressed=false;
        }
        evt.preventDefault();
    });
    domEl.addEventListener("mousemove",(evt)=>{
        mouse.x=event.clientX;
        mouse.y=event.clientY;
        evt.preventDefault();
        if(mouse.isSelectingArea){
            areaSelectionMove();
        }else{
            mouse.isDragging.forEach((handle)=>{
                handle.dragHandler();
            });
        }
    
    });

}
export default Dragaround;