
import El from './El.js';
import $ from "jquery";

var ToggleButton=function(label="toggle"){
    const self=this;
    this.active=false;
    this.$el=$("");
    El.call(this,`<div class="button toggle">${label}</div>`);
    this.$el.on("click",function(){
        self.active=!self.active;
        self.$el.removeClass((!self.active)+"");
        self.$el.addClass((self.active)+"");
        self.emit("toggled",self.active);
    });

}
ToggleButton.list=[];

export default ToggleButton;