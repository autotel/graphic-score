import El from "./El";
import $ from "jquery";
import Dragaround from "./Dragaround";

import getAudioContext from "./getAudioContext";

const context=getAudioContext();

function getProgress(){
    return context.currentTime;
}

const Board=function(){
    const myBoard=this;
    El.call(this,`<div class="board"></div>`);
    const drag=new Dragaround(this.$el[0]);
    
    
    const Line=function(){
        El.call(this,`<div class="line"></div>`);
        
        const redrawList=new Set();
        let svgWidth=700;
        let svgHeight=700;
        const $svgContainer=$(`
        <svg class="composition-line" width="${svgWidth}" height="${svgHeight}">
            <path
                d="M2 1 h1 v1 h1 v1 h-1 v1 h-1 v-1 h-1 v-1 h1 z" />
        </svg>`);
        const $path=$svgContainer.find("path");
        $svgContainer.appendTo(this.$el);

        //variables that help mapping position to time
        let compositionDuration;
        let pixelDuration;
        let pixelRatio;
        const updateSize=()=>{
            $svgContainer.attr("width",svgWidth);
            $svgContainer.attr("height",svgHeight);
            compositionDuration=10000;
            pixelDuration=compositionDuration/svgWidth;
            pixelRatio=1/svgHeight;
            console.log({svgWidth,svgHeight,pixelRatio,pixelDuration});
        }
        updateSize();

        var handles=[];
        
        this.getLine=()=>handles.sort((a,b)=>b.position.x-a.position.x);

        const redraw=()=>{
            const line=this.getLine();
            const steps=line.length;
            let svgString="";
            for(let index=0; index<steps; index+=1){
                const {x,y}=line[index].position;
                //parse the string and add
                if(index==0){
                    svgString+=`M${x},${y}`;
                }else{
                    svgString+=`\nL${x},${y}`;
                }
            }
            $path.attr("d",svgString);
            redrawList.forEach((a)=>a());
        }

        this.getComposition=()=>{
            const line=this.getLine();
            return line.map(
                ({position:{x,y}})=>
                   ({start:x*pixelDuration,level:1-(y*pixelRatio)})
            );
        }

        const addHandle=(coords)=>{
            const h=drag.createHandle(coords);
            const $extender=$(`<div class="extender"></div>`);
            $extender.appendTo(this.$el);


            $extender.on("mousedown",()=>{
                const offset=myBoard.$el.offset();
                addHandle({
                    x:drag.mouse.x-offset.left,
                    y:drag.mouse.y-offset.top,
                }).startDragging();
            });

            h.onDrag=()=>redraw();

            const redFn=function(pos){
                return function(){
                    $extender.css({
                        left:pos.x+"px",
                        top:pos.y+"px"
                    });
                }
            };
            redrawList.add(redFn(h.position));

            h.onDragEnd=({x,y})=>{
                redraw();
                compositionChangeHandler();
            }
            // $(h.domElement).contextmenu(()=>false);
            $(h.domElement).on("mouseenter",function(event) {
                // .backgroundColor="black"
                // $(this).css("background-color","black");
            });
            $(h.domElement).on("mouseleave",function(event) {
                // .backgroundColor="black"
                // $(this).css("background-color","");
            });
            $(h.domElement).on("mouseup",function(event) {
                console.log(event.button);
                // if(event.button==2){
                //     handles.splice(handles.indexOf(h),1);
                //     h.remove();
                //     $extender.remove();
                //     redraw();
                //     event.stopPropagation();
                //     event.preventDefault();
                //     redrawList.remove(redFn);
                //     return false;
                // }
            });
            handles.push(h);
            redraw();
            return h;
        }

        addHandle({x:Math.random()*600,y:Math.random()*svgHeight});
        addHandle({x:Math.random()*600,y:Math.random()*svgHeight});
    }

    const addLine = (className)=>{
        const ret=new Line();
        ret.$el.appendTo(this.$el);
        ret.$el.addClass(className);
        return ret;
    }

    const lines={
        speed:[addLine("chan-a-speed"),addLine("chan-b-speed")],
        vol:[addLine("chan-a-vol"),addLine("chan-b-vol")],
    }

    const compositionChangeHandler=()=>{
        let speeds=lines.speed.map((line)=>line.getComposition());
        let vols=lines.vol.map((line)=>line.getComposition());
        const compositions=[];

        speeds.map((composition,index)=>{
            compositions[index]={
                pitch:composition,
            };
        });
        vols.map((composition,index)=>{
            if(!compositions[index])compositions[index]={}
            compositions[index].reverb=composition;
        });
        myBoard.emit("composition-changed",compositions);
    }
    
};
export default Board;