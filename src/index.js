import El from './El.js';
import $ from "jquery";
import Wave from './WaveSurfer';
import SoundLoader from "./SoundLoader";
import SoundWheel from './SoundWheel.js';
import Board from './Board.js';
var $root=$("#playground");
$root.html("");
var StartButton=function(){
    El.call(this,`<div class="button">
    Start
    </div>`);
    this.$el.on("click",function(){
        console.log("click");
    });
}

window.addEventListener('load', (event) => {
    /**
     * @type {AudioBuffer|false} 
     */
    let currentWave=false;

    let $firstRow=$(`<div class="row"></div>`);
    let $secondRow=$(`<div class="row"></div>`);
    let $thirdRow=$(`<div class="row"></div>`);
    
    let $channelsContainer = $(`<div class="channels-container"></div>`);
    
    $root.append([$firstRow,$secondRow,$thirdRow]);
    
    let button=new StartButton().appendTo($firstRow);
    let loader=new SoundLoader().appendTo($firstRow);
    
    let board=new Board().appendTo($secondRow);
    let wave=new Wave().appendTo($thirdRow);
    
    $channelsContainer.appendTo($secondRow);

    wave.on("region-created",(...p)=>{
        // console.log("reg",...p);
        regionChangeFunction(p[0]);
    });
    wave.on("region-updated",(...p)=>{
        // console.log("reg",...p);
        regionChangeFunction(p[0]);
    });
    

    let chans={
        A:new SoundWheel(),
        B:new SoundWheel(),
    }
    board.on("composition-changed",(compositions)=>{
        console.log("change",compositions);
        chans.A.applyEnvelopes(compositions[0]);
        chans.B.applyEnvelopes(compositions[1]);
    })

    function regionChangeFunction(region){
        if(!currentWave) return;
        const waveArray=currentWave.getChannelData(0);
        const sampleRate=currentWave.sampleRate;
        const startSample=region.start*sampleRate;
        const endSample=region.end*sampleRate;
        const portion=waveArray.slice(startSample,endSample);
        chans[region.id].set(portion,sampleRate);
    }

    Object.keys(chans).map(chanName=>chans[chanName].appendTo($channelsContainer));
    
    loader.on("load",function({audioArrayBuffer,decodedAudio}){
        // console.log("load listener",{audioArrayBuffer,decodedAudio});
        currentWave=decodedAudio;
        wave.set(audioArrayBuffer);
    });
    
    loader.on("read",function(audioArrayBuffer){
        currentWave=audioArrayBuffer;
        wave.set(audioArrayBuffer);
    });
});

