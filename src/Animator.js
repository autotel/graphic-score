
const animator=new(function(){
    
    if(!window.animatorCount)window.animatorCount=0;
    window.animatorCount++;
    if(window.animatorCount>1){
        throw new Error("more than one animator");
    }

    this.list=[];

    const redraw=(timestamp)=>{
        this.list.map((fn)=>fn(timestamp));
        requestAnimationFrame(redraw);
    }
    this.addFrameFunction=(fn)=>{
        this.list.push(fn);
    }
    this.removeFrameFunction=(fn)=>{
        this.list.splice(this.list.indexOf(fn),1);
    }
    redraw();
})();
export default animator;