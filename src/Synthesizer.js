import getAudioContext from "./getAudioContext";

const context=getAudioContext();

const Synthesizer=function(){
    const grains=[];

    var source = context.createBufferSource();
    source.connect(context.destination);


    /** 
     * define the sound source for the grain number... the voice number
     * @param audio audio, have to decide the format
     * @param {number} number which grain to set. 
     */
    this.setGrain=function(audio,number){
        grains[number]=audio;


        /* --- play the sound AFTER the buffer loaded --- */
        //set the buffer to the response we just received.
        source.buffer = audio;
        //start(0) should play asap.
        source.start(0);
        source.loop = true;
    }
    /**
     * trigger an attack
    */
    this.start=function(){}

    /**
     * trigger a release
    */
    this.stop=function(){}
}
export default Synthesizer;

//https://stackoverflow.com/questions/29882907/how-to-seamlessly-loop-sound-with-web-audio-api