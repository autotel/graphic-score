import El from "./El";
import $ from "jquery";
import WaveSurfer from "./WaveSurfer";
import getAudioContext from "./getAudioContext";
import ToggleButton from "./ToggleButton";
import animator from "./Animator";

const context=getAudioContext();

function timeFromNow(time){
    return context.currentTime+time;
}
const SoundWheel=function(){
    const self=this;
    let isPlaying=false;
    let normalize=true;
    let envelope=false;
    let tesselate=true;

    const outGain = context.createGain();
    const dryGain = context.createGain();
    const wetGain = context.createGain();

    const label=(["A","B","C","D"])[SoundWheel.list.length];
    SoundWheel.list.push(this);
    El.call(this,`<div class="channel"></div>`);
    // const self=this;
    const $picture=$(`<img class="bg" src="./media/button-${label}.png"/>`);
    const $button=$(`<div class="graph-button"></div>`);
    const $label=$(`<p class="label-button">${label}</p>`);

    let svgWidth=200;
    let svgHeight=svgWidth;
    let svgHalf=svgWidth/2;
    const $svgContainer=$(`
        <div class="svg-container">
            <div class="speedy" style="
                width:${svgWidth*0.7}px;
                height:${svgWidth*0.7}px;
                top:50%;
                left:50%;
                margin-left:-${svgWidth*0.35}px;
                margin-top:-${svgWidth*0.35}px;
            ">
                <img class="graph"/>
            </div>
            <svg class="wave" viewBox="0 0 ${svgWidth} ${svgHeight}">
                
                <path
                    class="waveform"
                    style="transform-origin: center;"
                    d="M2 1 h1 v1 h1 v1 h-1 v1 h-1 v-1 h-1 v-1 h1 z" 
                />
            </svg>
        </div>
    `);
    const $path=$svgContainer.find(".waveform");
    // const $circle=$svgContainer.find(".circle");
    const $circle=$svgContainer.find(".speedy");
    const $circleImage=$circle.find(".graph");
    $circleImage.appendTo($circle);
    $circle.appendTo($svgContainer);
    var $slider=$(`<input data-action="vol" step="0.01" type="range" min="0" max="2" value="0.2" style="width: 100%">`);

    var $controlContainer=$(`<div></div>`);
    var $useNormalize=new ToggleButton('Normalize');
    var $useXcrossingButton=new ToggleButton('Xcrossing');
    var $useTesselateButton=new ToggleButton('Tesselate');
    var $useEnvelopeButton=new ToggleButton('Envelope');

    $controlContainer.append([
        $useXcrossingButton.$el,
        $useTesselateButton.$el,
        $useEnvelopeButton.$el,
        $useNormalize.$el,
    ]);

    $useNormalize.on("toggled",function(newValue){
        normalize=newValue;
        self.set();
    });
    $useEnvelopeButton.on("toggled",function(newValue){
        envelope=newValue;
        self.set();
    });
    $useTesselateButton.on("toggled",function(newValue){
        tesselate=newValue;
        self.set();
    });

    $controlContainer.appendTo(self.$el);

    $slider.on('input',function (evt) {
        // console.log(evt);
        changeVolume(Number(this.value));
    });
    changeVolume(Number($slider.attr("value")));

    $slider.appendTo(self.$el);
    $button.appendTo(self.$el);
    $picture.appendTo($button);
    $label.appendTo($button);
    $svgContainer.appendTo(this.$el);

    let svgString="";


    //reverb js (included in index.html);
    if(reverbjs){
        console.log("using reverb");
        let reverbUrl = "media/slinky_ir.wav";
        reverbjs.extend(context);
        let reverbNode = context.createReverbFromUrl(reverbUrl, function() {
            reverbNode.connect(context.destination);
        });
        wetGain.connect(reverbNode);
    }else{
        console.log("skipping reverb, it is ",reverbjs);
    }
    dryGain.connect(context.destination);
    outGain.connect(wetGain);
    outGain.connect(dryGain);

    function changeVolume(to){
        console.log("volume",to);
        // dryGain.gain.setValueAtTime(0, to);
        outGain.gain.value=to;
    }
    function updateSvg(array){
        
        //maximum wave points to draw
        const steps=900;
        //length of the audio segment to draw
        let length=array.length;
        //how many samples to skip on each drawing point
        let stepsPerValue=length/steps;
        //how big to draw the wave, in pixels
        let waveRangePx=svgWidth/30;
        //center of the circle
        const centerx=svgHalf;
        const centery=svgHeight/2;
        //radius of the circle. The wave is drawn around it
        const circleRadius=centerx - waveRangePx;
        //precalculation 
        const twoPi=Math.PI*2
        //the string that makes the drawing
        svgString="";
        //within the array, get the average level from start to end
        function getRep(start,end){
            let total=0;
            let fac=1/(end-start);
            // array.slice(start,end).map((val)=>total+=Math.sqrt(val*val));
            // total*=fac;
            array.slice(start,end).map((val)=>{
                const tv=Math.abs(val)
                if(tv>total)total=val;
            });
            return total;
        }
        for(let x=0; x<steps; x+=1){
            //index in the audio array
            let index=Math.floor(x*stepsPerValue)
            //wave voltage level, cartesian
            let y=0;
            if(stepsPerValue>1){
                y=getRep(index,Math.floor(index+stepsPerValue))*waveRangePx-waveRangePx*0.5;
            }else{
                y=array[index]*waveRangePx-waveRangePx*0.5;
            }
            //precalculation
            let sineIndex = twoPi * x /steps;
            //cartesian to polar
            let polarx=Math.cos(sineIndex) * (circleRadius + y);
            let polary=Math.sin(sineIndex) * (circleRadius + y) ;
            //parse the string and add
            if(index==0){
                svgString+=`M${polarx + centerx},${polary + centery}`;
            }else{
                svgString+=`\nL${polarx + centerx},${polary + centery}`;
            }
        }
        //indicate shape should be closed
        svgString+=`\nZ`;
        $path.attr("d",svgString);
    }
    let lastDraw=0;
    let angle=0;
    const frame=(time)=>{
        const deltaTime=time-lastDraw;
        // console.log("f",1/deltaTime);
        lastDraw=time;
        
        if(source){
            
            let animRate=source.playbackRate.value/currentSampleDuration;
            const angleMult=0.36;

            // $path.css({transform:`rotateZ(${angle*angleMult}deg)`});
            $circle.css({
                transform:`rotateZ(${angle*angleMult}deg)`
            });

            
            let speedyGraph="media/speedy-"+(
                Math.min(
                    Math.floor(animRate),
                    3
                )
            )+".png";

            $circleImage.attr("src",speedyGraph);


            angle+=animRate*deltaTime;
        }
    }
    animator.addFrameFunction(frame);


    $path.attr("d",svgString);
    /**
     * @type {AudioBufferSourceNode|false}
     */
    let source=false;
    /**
     * current audio array, without modifications
     * @type {number[]|false}
     */
    let currentAudioArray=false;
    let currentProcessedAudioArray=false;
    let currentSampleRate=44100;
    let currentSampleDuration=0;
    let envelopeStarted=0;

    this.set=function(
        audioArray=currentAudioArray,
        sampleRate=currentSampleRate
    ){
        if(!audioArray) return;
        currentProcessedAudioArray=audioArray.slice(0);
        currentAudioArray=audioArray.slice(0);

        if(tesselate){
            const length=currentProcessedAudioArray.length;
            const halfLength=Math.round(length/2);
            const audioCopy=currentProcessedAudioArray.slice(0);
            for(let a=0; a<length;a++){
                const alevel=Math.cos(2 * Math.PI * a/length)+1;
                const level=1-alevel;
                
                currentProcessedAudioArray[a]*=level;
                
                if(a<halfLength){
                    currentProcessedAudioArray[a]+=audioCopy[a+halfLength]*alevel;
                }else if(a>halfLength){
                    currentProcessedAudioArray[a]+=audioCopy[a-halfLength]*alevel;
                }
                if(isNaN(currentProcessedAudioArray[a])){
                    // console.log("nan",`currentProcessedAudioArray[${a}]=${currentProcessedAudioArray[a]}`);
                    currentProcessedAudioArray[a]=0;
                }
                if(isNaN(level)){
                    // console.log("nan",`1-${alevel}`);
                }
                if(isNaN(alevel)){
                    // console.log("nan",`Math.cos(Math.PI * ${a}/${halfLength})+1`);
                }
            }
        }
        if(normalize){
            const multiply=
                1/
                Math.max(
                    Math.max(...currentProcessedAudioArray),
                    Math.abs(Math.min(...currentProcessedAudioArray))
                );
            currentProcessedAudioArray=currentProcessedAudioArray.map((a)=>a*multiply);
        }
        if(envelope){
            const envelopeLength=400;
            for(let a=0; a<envelopeLength;a++){
                currentProcessedAudioArray[a]*=a/envelopeLength;
                currentProcessedAudioArray[currentProcessedAudioArray.length-a]*=a/envelopeLength;
            }
        }

        updateSvg(currentProcessedAudioArray);

        restart();

    }
    $button.on("click",()=>{
        if(isPlaying){
            console.log("stop");
            stop();
        }else{
            console.log("start");
            restart();
        }
        self.set();
        $button.removeClass((!isPlaying)+"");
        $button.addClass((isPlaying)+"");
    });

    let currentEnvelopes=false;
    /**
     * @typedef {{start:number,level:number}[]} envelopeList;
     * @param {{pitch:envelopeList,reverb:envelopeList}} composition
     */
    this.applyEnvelopes=(envelopes)=>{
        console.log("applyPitchEnvelope");
        currentEnvelopes=envelopes;
    }
    function stop(){
        isPlaying=false;
        if(source){
            try{
                source.disconnect();
                source.stop();
            }catch(e){}
        }
    }
    function restart(){
        const envelopes=currentEnvelopes;
        const now=context.currentTime;
        envelopeStarted=now;

        stop();
        console.log({
            length: currentProcessedAudioArray.length,
            sampleRate: currentSampleRate,
            numberOfSoundWheels: 1,
        });

        const b = new AudioBuffer({
            length: currentProcessedAudioArray.length,
            sampleRate: currentSampleRate,
            numberOfSoundWheels: 1,
        });


        currentSampleDuration=currentProcessedAudioArray.length/currentSampleRate;

        b.copyToChannel(new Float32Array(currentProcessedAudioArray),0,0);
        source = new AudioBufferSourceNode(context, {
            buffer: b, 
        });

        if(!source) throw new Error("failed to make source");
        source.connect(outGain);
        source.loop = true;        
        console.log("restart");
        isPlaying=true;
        source.start(0);

        envelopes.pitch.sort((a,b)=>a.start-b.start);

        //silence sample at the times where there is no envelope line
        const pitchStart=envelopes.pitch[0].start;
        const pitchEnd=envelopes.pitch[envelopes.pitch.length-1].start;
        //make it start silent
        outGain.gain.setValueAtTime(0,now);
        //200 ms attack
        outGain.gain.setValueAtTime(0,now+(pitchStart/1000));
        outGain.gain.linearRampToValueAtTime(0.7,now+(pitchStart/1000)+0.2);
        //200 ms end
        outGain.gain.setValueAtTime(0.7,now+(pitchEnd/1000));
        outGain.gain.linearRampToValueAtTime(0,now+(pitchEnd/1000)+0.2);

        
        envelopes.pitch.map(({start,level})=>{
            console.log("pitch",{start,level});
            try{
                source.playbackRate.linearRampToValueAtTime(
                    Math.pow(2,level),now+(start/1000)
                );
            }catch(e){
                console.error(e);
            }
        });

        envelopes.reverb.map(({start,level})=>{
            console.log("reverb",{start,level});
            try{
                let alevel=1-level;
                wetGain.gain.linearRampToValueAtTime(
                    level,now+(start/1000)
                );
                dryGain.gain.linearRampToValueAtTime(
                    alevel,now+(start/1000)
                );
            }catch(e){
                console.error(e);
            }
        });

    }
    this.getPlayHead=function(){
        return context.currentTime - envelopeStarted;
    }

}
SoundWheel.list=[];

export default SoundWheel;
